#include "Pose.h"
#include<math.h>
#include<iostream>
#include<stdlib.h>
#include<conio.h>

using namespace std;

int main()
{

	Pose p1;

	p1.setPose(10, 20, 60); //setPose function test  (includes setX() , setY() and setTh())

	cout << "for p1: ";
	cout << " X is set as " << p1.getX() << endl;
	cout << " Y is set as " << p1.getY() << endl;
	cout << " Th is set as " << p1.getTh() << endl;

	cout << endl << endl;



	cout << " So the Position is: "; p1.printPose();  //printPose function test  (includes getX() , getY() and getTh())

	cout << endl << endl;



	Pose p2;

	p2.setPose(30, 40, 150);

	cout << "for p2: ";
	cout << " X is set as " << p2.getX() << endl;
	cout << " Y is set as " << p2.getY() << endl;
	cout << " Th is set as " << p2.getTh() << endl;

	cout << endl << endl;

	cout << "Distance between p1 and p2 is: " << p1.findDistanceTo(p2); //findDistance function test

	cout << endl << endl;

	cout << "Angle between p1 and p2 is: " << p1.findAngelTo(p2);    //findAngle function test

	cout << endl << endl;

	Pose p3(10, 20, 60);

	cout << "for p3: ";
	cout << " X is set as " << p3.getX() << endl;
	cout << " Y is set as " << p3.getY() << endl;
	cout << " Th is set as " << p3.getTh() << endl;

	cout << endl << endl;

	float x, y, th;
	p3.getPose(x, y, th);

	cout << "getPose function test... " << x << " " << y << " " << th;

	cout << endl << endl;

	if (p1 == p2)  // operator == overloading test
	{
		cout << "p1 and p2 are equal";

	}
	else
	{
		cout << "p1 and p2 are not equal";
	}

	cout << endl << endl;

	if (p1 == p3) // operator == overloading test
	{
		cout << "p1 and p3 are equal";


	}
	else
	{
		cout << "p1 and p3 are not equal";
	}
	cout << endl << endl;



	Pose pResult = p1 + p2; // operator + overloading test

	cout << "Result of p1+p2: ";
	pResult.printPose();


	cout << endl << endl;

	cout << "Result of p1-p2: ";
	pResult = p1 - p2;  // operator - overloading test

	pResult.printPose();


	cout << endl << endl;

	cout << "after p3 += p2, p3: ";
	(p3 += p2).printPose();  // operator += overloading test

	cout << endl << endl;


	cout << "after p3 -= p2, p3: ";
	(p3 -= p2).printPose();  // operator -= overloading test


	cout << endl << endl;


	if (p1 < p2) //  operator < overloading test (magnitude of the position depends on distance from origin
	{
		cout << "p1 is smaller than p2";

	}
	else
	{
		cout << "p1 is not smaller than p2";
	}

	cout << endl << endl;

	if (p2 < p1)
	{
		cout << "p2 is smaller than p1";
	}
	else
	{
		cout << "p2 is not smaller than p1";
	}

	cout << endl << endl << endl << endl;


	cout << "Dont forget to scroll up :)" << endl << endl;
	cout << "Press any key to exit";
	_getch();
	return 1;
}