#pragma once
#include"Node.h"
#include"Pose.h"
#include<iostream>
using namespace std;
class Path 
{
private:
	Node * tail;
	Node * head;
	int number;
public:
	Path();
	~Path();
	void addPos(Pose pose);
	void print();
	Pose getPos(int index);
	bool removePos(int index);
	bool insertPos(int index, Pose pose);
	friend ostream& operator<<(ostream&,const Path&);
	friend istream& operator>>(istream&,Pose &/*pose*/);
	int oparator[](int index);

};