#pragma once
#include "PioneerRobotAPI.h"
class SonarSensor
{
private:
	float ranges[16];
	PioneerRobotAPI *robotAPI;
public:
	float getRange(int index);
	float getMax(int& index);
	float getMin(int& index);
	void updateSensor(float ranges[]);
	float operator[](int i);
	float getAngle(int index);
};

