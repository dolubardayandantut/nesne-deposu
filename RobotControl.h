#pragma once
#include "PioneerRobotAPI.h"
#include<iostream>
using namespace std;
class RobotControl
{
private:
	Pose* position;
	PioneerRobotAPI* robotAPI;
	int state;
public:
	RobotControl();
	~RobotControl();
	void turnLeft();
	void turnRight();
	void forward(float speed);
	void print();
	void backward(float speed);
	Pose getPose();
	void setPose(Pose);
	void stopTurn();
	void stopMove();
};