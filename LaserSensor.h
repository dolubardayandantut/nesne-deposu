#pragma once
#include"PioneerRobotAPI.h"

class LaserSensor
{
public:
	LaserSensor();
	~LaserSensor();
	float getRange(int);
	void updateSensor(float[]);
	float getMax(int &);
	float getMin(int &);
	float operator[](int);
	float getAngle(int);
	float getClosestRange(float, float, float&);
private:
	PioneerRobotAPI* robotAPI;
	float ranges[181];
	float min;
	float max;
};

