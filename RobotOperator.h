#pragma once
#include"iostream"
#include"string"
using namespace std;
#include"Encryption.h"
class RobotOperator
{
public:
	RobotOperator();
	void getSurname();
	void setSurname();
	void getName();
	bool checkAccessCode(int);
	void print();
	~RobotOperator();
private:
	string name;
	string surname;
	unsigned int accessCode;
	bool accesState;
	int encryptCode(int);
	int decryptCode(int);

	
};

