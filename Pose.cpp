#include "Pose.h"
#include<math.h>
#include<iostream>
#include<stdlib.h>

using namespace std;


Pose::Pose(float _x, float _y, float _th)
{
	x = _x;
	y = _y;
	th = _th;

}

Pose::~Pose()
{
}


float Pose::getX()const
{
	return x;
}

void Pose::setX(float _x)
{
	x = _x;


}

float Pose::getY()const
{
	return y;
}

void Pose::setY(float _y)
{
	y = _y;


}

float Pose::getTh()const
{
	return th;

}


void Pose::setTh(float _th)
{
	if (_th < 0)
	{
		_th = 360 - _th;
	}
	if (_th > 360)
	{
		_th = _th - 360;
	}
	th = _th;


}

bool Pose::operator==(const Pose &other)
{
	if (x == other.getX() && y == other.getY() && th == other.getTh())
	{
		return true;
	}

	return false;
}

Pose Pose::operator+(const Pose &other)
{
	Pose temp;
	temp.x = x + other.getX();
	temp.y = y + other.getY();

	temp.th = th + other.getTh();

	if (temp.th >= 360)
	{
		temp.th = temp.th - 360;
	}

	return temp;
}

Pose Pose::operator-(Pose & const other)
{
	Pose temp;
	temp.x = x - other.getX();
	temp.y = y - other.getY();

	temp.th = th - other.getTh();



	if (temp.th < 0)
	{
		temp.th = 360 + temp.th;
	}

	return temp;
}

Pose & Pose::operator+=(Pose & const other)
{
	x = x + other.getX();
	y = y + other.getY();
	th = th + other.getTh();


	return *this;
}

Pose & Pose::operator-=(Pose & const other)
{
	x = x - other.getX();
	y = y - other.getY();
	th = th - other.getTh();


	return *this;
}

bool Pose::operator < (Pose & const other)
{
	float first = x*x + y*y;
	float second = other.x*other.x + other.y*other.y;

	if (first < second)
	{
		return true;
	}

	return false;
}



void Pose::setPose(float _x, float _y, float _th)
{
	setX(_x);
	setY(_y);
	setTh(_th);
}

void Pose::getPose(float & _x, float & _y, float & _th) const
{
	_x = x;
	_y = y;
	_th = th;
}

float Pose::findDistanceTo(Pose pose)
{
	return sqrt((x - pose.x)*(x - pose.x) + (y - pose.y)*(y - pose.y));
}

float Pose::findAngelTo(Pose pose)
{
	float _th;
	_th = th - pose.th;
	if (_th < 0)
	{
		_th = -1 * _th;
	}
	return _th;
}

void Pose::printPose()
{
	cout << " X: " << this->getX() << " Y: " << this->getY() << " Th: " << this->getTh();
}
