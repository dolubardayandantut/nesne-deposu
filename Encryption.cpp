#include "Encryption.h"
#include"iostream"

using namespace std;



Encryption::Encryption()
{
}
Encryption::~Encryption()
{
}
int encrypt(int input) {
	int input_array[4];


	input_array[0] = input % 10;
	input_array[1] = (input % 100 - input_array[0]) / 10;
	input_array[2] = (input % 1000 - input_array[1] - input_array[0]) / 100;
	input_array[3] = (input % 10000 - input_array[2] - input_array[1] - input_array[0]) / 1000;

	for (int i = 0; i < 4; i++) {
		input_array[i] = (input_array[i] + 7 ) % 10;
	}
	return (input_array[3] * 1000 + input_array[2] * 100 + input_array[1] * 10 + input_array[0]);

}
int decrypt(int input){
	int input_array[4];

	input_array[0] = input % 10;
	input_array[1] = (input % 100 - input_array[0]) / 10;
	input_array[2] = (input % 1000 - input_array[1] - input_array[0]) / 100;
	input_array[3] = (input % 10000 - input_array[2] - input_array[1] - input_array[0]) / 1000;

	for (int i = 0; i < 4; i++) {
		input_array[i] = (input_array[i] + 3) % 10;
	}
	return (input_array[3] * 1000 + input_array[2] * 100 + input_array[1] * 10 + input_array[0]);
}
