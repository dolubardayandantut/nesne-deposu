#ifndef CLASS_POSE

#define CLASS_POSE


class Pose
{
	float x;
	float y;
	float th;
public:
	Pose(float = 0, float = 0, float = 0);
	~Pose();

	float getX()const;
	void setX(float);
	float getY()const;
	void setY(float);
	float getTh()const;
	void setTh(float);
	void setPose(float, float, float);
	void  getPose(float &, float &, float &) const;

	void printPose();

	float findDistanceTo(Pose);
	float findAngelTo(Pose);

	bool operator == (const Pose&);
	Pose operator + (const Pose&);
	Pose operator - (Pose& const);
	Pose& operator += (Pose& const);
	Pose& operator -= (Pose& const);
	bool operator < (Pose& const);




};

#endif 