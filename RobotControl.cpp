#include "RobotControl.h"
RobotControl::RobotControl()
{
}
RobotControl::~RobotControl()
{
}

void RobotControl::turnLeft()
{
	robotAPI->turnRobot(PioneerRobotAPI::left);
}
void RobotControl::turnRight()
{
	robotAPI->turnRobot(PioneerRobotAPI::right);
}
void RobotControl::forward(float speed)
{
	robotAPI->moveRobot(50);
}
void RobotControl::print()
{
	
}
void RobotControl::backward(float speed)
{
	robotAPI->moveRobot(-50);
}

void RobotControl::stopTurn()
{
	robotAPI->stopRobot();
}

void RobotControl::stopMove()
{
	robotAPI->stopRobot();
}


